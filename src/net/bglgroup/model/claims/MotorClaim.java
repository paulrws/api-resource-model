package net.bglgroup.model.claims;

import java.math.BigDecimal;
import java.util.Calendar;

import net.bglgroup.model.person.InsuredPerson;

public class MotorClaim {
	private String type;
	private Calendar date;
	private InsuredPerson driver;
	private String atFault;
	private Boolean injuries;
	private Boolean claimMade;
	private String damageLevel;
	private BigDecimal value;
}
