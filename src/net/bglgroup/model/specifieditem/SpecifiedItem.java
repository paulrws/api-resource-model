package net.bglgroup.model.specifieditem;

import java.math.BigDecimal;

public class SpecifiedItem {

	private String type; 
	private String description;
	private BigDecimal marketValue;
	private Boolean evidenceOfValue;
}
