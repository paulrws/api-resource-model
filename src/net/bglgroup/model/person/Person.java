package net.bglgroup.model.person;
import java.util.Date;

import net.bglgroup.model.Party;
import net.bglgroup.model.address.Address;

public class Person implements Party {
	private String title;
	private String firstName;
	private String lastName;
	private Date dateOfBirth;
	private String gender;
	private String maritalStatus;
	private String employmentStatus;
	private String occupation;
	private String businessCategory;
	
	//ContactDetails
	private Address postalAddress;
	private String emailAddress;
	private String homePhonenumber;
	private String mobilePhonenumber;
}
