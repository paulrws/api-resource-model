package net.bglgroup.model.person;
import java.util.List;

import net.bglgroup.model.claims.MotorClaim;
import net.bglgroup.model.convictions.Conviction;
import net.bglgroup.model.medicalconditions.MedicalCondition;

public class InsuredPerson extends Person {

	private List<PersonRelationship> relationships;
	private List<MotorClaim> motorClaims;
	private List<Conviction> convictions;
	private List<MedicalCondition> medicalConditions;
}
