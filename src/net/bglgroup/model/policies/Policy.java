package net.bglgroup.model.policies;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;

import net.bglgroup.model.covers.Cover;

public abstract class Policy {

	private String status;
	private Calendar startTimestamp;
	private Calendar expiryTimestamp;
	private Calendar cancellationTimestamp;
	private String policyDuration;
	private String premiumDuration;
	private BigDecimal totalPremium;
	private List<Cover> covers;
}
