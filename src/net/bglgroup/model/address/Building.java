package net.bglgroup.model.address;

public class Building {

	private String type;
	private Integer numberOfBedrooms;
	private String wallConstruction;
	private String roofConstruction;
	private Integer flatRoofPercentage;
	private Integer yearBuilt;
	private String ownershipStatus;
	private String residenceStatus;
	private String occupancyType;
}
