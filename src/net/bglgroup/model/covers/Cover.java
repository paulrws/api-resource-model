package net.bglgroup.model.covers;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;

import net.bglgroup.model.excess.Excess;

public abstract class Cover {
	
	private String status;
	private String level;
	private BigDecimal insuredValue;
	private Boolean notionalValue;
	private Calendar startTimestamp;
	private Calendar cancellationTimestamp;
	private BigDecimal totalPremium;
	private BigDecimal totalExcess;
	private List<Excess> excesses;
}
