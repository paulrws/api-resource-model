package net.bglgroup.model.covers.motor;

import java.util.List;

import net.bglgroup.model.covers.Cover;
import net.bglgroup.model.driver.Driver;

public abstract class MotorCover extends Cover {

	private String driverOption;
	private String classOfUse;
	private Integer annualMileage;
	private Integer ncdYears;
	private Boolean ncdStarter;
	private Boolean thirdPartyExtension;
	private Boolean mandatory;
	private List<Driver> drivers;
}
