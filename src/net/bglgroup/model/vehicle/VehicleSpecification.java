package net.bglgroup.model.vehicle;
public class VehicleSpecification {

	private String description;
	private String manufacturer;
	private String model;
	private String trim;
	private String bodyType;
	private Integer doors;
	private String fuelType;
	private String engineSize;
	private String transmissionType;
	private String registrationYear;
	private String registrationLetter;
}
