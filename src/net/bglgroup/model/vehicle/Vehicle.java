package net.bglgroup.model.vehicle;
import java.util.Date;

import net.bglgroup.model.Party;
import net.bglgroup.model.address.Address;

public abstract class Vehicle {
	private String registrationNumber;
	private Boolean leftHandDrive;
	private Integer numberOfSeats;
	private String bodyType;
	private Date purchaseDate;
	private String overnightParking;
	private Address overnightAddress;
	private Party registeredKeeper;
	private Party registeredOwner;
	private VehicleSpecification specification;
}
